%define debug_package %{nil}

Name: fzf
Version: 0.53.0
Release: 1%{?dist}
Summary: fzf is a general-purpose command-line fuzzy finder.
License: MIT
URL: https://github.com/junegunn/fzf
Source0: https://github.com/junegunn/%{name}/archive/%{version}.tar.gz#/%{name}-%{version}.tar.gz

BuildRequires: golang >= 1.20

%description
fzf is a general-purpose command-line fuzzy finder.

It's an interactive filter program for any kind of list; files, command history,
processes, hostnames, bookmarks, git commits, etc. It implements a "fuzzy"
matching algorithm, so you can quickly type in patterns with omitted characters
and still get the results you want.

%prep
%autosetup -n %{name}-%{version} -p1

%build
go env -w GO111MODULE=on
go env -w GOPROXY=https://goproxy.io,direct
go build -a -ldflags "-s -w -X main.version=%{version} -X main.revision=openEuler"

%install
install -Dm755 fzf %{buildroot}%{_bindir}/fzf
install -Dm644 man/man1/fzf.1 %{buildroot}%{_mandir}/man1/fzf.1

# tmux
install -Dm644 man/man1/fzf-tmux.1 %{buildroot}%{_mandir}/man1/fzf-tmux.1
install -Dm755 bin/fzf-tmux %{buildroot}%{_bindir}/fzf-tmux

# shell completions
install -Dm0644 shell/completion.bash \
    %{buildroot}%{_datadir}/bash-completion/completions/fzf
install -Dm0644 shell/key-bindings.bash \
    %{buildroot}%{_datadir}/bash-completion/completions/fzf-key-bindings

%files
%{_bindir}/fzf
%{_mandir}/man1/fzf.1.gz

%license LICENSE
%{_bindir}/fzf-tmux
%{_mandir}/man1/fzf-tmux.1.gz

%{_datadir}/bash-completion/completions/fzf
%{_datadir}/bash-completion/completions/fzf-key-bindings

%changelog
